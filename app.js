var   express = require('express')
    , bodyParser = require('body-parser')
    , methodOverride = require('method-override')
    //, http = require('http')
    , app = express()
    , port = parseInt(process.env.PORT, 10) || 8080
    , ip = process.env.IP
    ;
    
app.set('port', port);
app.set('id',   ip);

//app.set('view engine', 'html');
app.engine('jade', require('jade').__express);
app.set('view engine', 'jade');

// set the static files location - serve right from here
app.use(express.static(__dirname));

// pull information from html in POST
app.use(bodyParser());
// simulate DELETE and PUT
app.use(methodOverride());

app.get(function (req, res, next) {
    res.render("index.html", {
        title: "Barash33p"
    });
});

app.listen(port, ip);
console.log("Magic happens at: " + ip + ":" + port);