// PubSub
var PubSub = {
    
    actionCallbacks: [],
    
    subscribe : function (actionName, callback) {
        if (!this.actionCallbacks[actionName]) {
            this.actionCallbacks[actionName] = [];
        }
        this.actionCallbacks[actionName].push(callback);
    },
    
    notify: function (actionName) {
        var args = Array.prototype.splice.call(arguments, 1),
            applyCallback = function (callback) {
                // call back the callbacks that were added to PubSub for this action Name.
                // console.log('Notifying ', actionName, " with these args: ", args);
                callback.apply(null, args);
            };

        for (var i in this.actionCallbacks) {
            if (this.actionCallbacks.hasOwnProperty(i) && i === actionName) {
                this.actionCallbacks[i].forEach(applyCallback);
            }
        }
    }

};