
/* global _ */

(function (_) {

    var root = this,
    
    Block = function (settings) {
        var self = this;
        self.setSettings(settings);
    
        self.settings.game.PubSub.subscribe("Character:Move:Right", function (params) {
            self.move(null, params);
        });
        if (self.type == "baseWater") {
            self.settings.game.PubSub.subscribe("Character:Jump", function (params) {
                self.leftUp = self.time = parseInt(params.time / 4, 10);
                console.log("w00t", self.leftUp, params.time);
            });
        }
    };
    
    Block.prototype.setSettings = function (settings) {
        var self = this;
        self.settings = settings;
        _.extend(self, settings);
    };
    
    Block.prototype.getBlockImageWidth = function () {
        return this.settings.canvas.w;
    };
    
    Block.prototype.getClientWidth = function () {
        return this.settings.game.settings.w;
    };
    
    Block.prototype.moveUp = function () {
        var self = this;
        self.time--;
        if (self.time == -self.leftUp) {
            delete(self.leftUp);
            return;
        }
        self.y = (self.time > 0 ? self.y - 1 : self.y + 1);
    };
    
    Block.prototype.move = function (time, params) {
        // block only moves when character reaches certain % of the screen
        // and notifies all the dependent elements through PubSub
    
        var self = this;
    
        /*
        if (self.type == "baseWater") {    
            if (self.hasOwnProperty("leftUp")) {
                self.moveUp();
            }
        }*/
    
    
        if (params) {
    
            if (self.type == "baseWater") {
                if (self.settings.level == 1) {
                    self.x -= params.speed * 0.44;
                } else if (self.settings.level == 2) {
                    self.x -= params.speed * 0.22;
                }            
            } else {
                self.x -= params.speed;
            }
        }
    
    };
    
    Block.prototype.draw = function (context, time) {
        var self = this,
            position = self.settings.canvas;
        self.move(time);
        context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
    };

    root.Block = Block;
    
}).call(this, _);