/* global requestAnimationFrame */
/* jshint loopfunc: true */

var Foreground = function (settings) {
  var self = this;
  self.settings = settings;
  self.foreground = document.querySelector(self.settings.foreground);
  self.cntx = self.foreground.getContext("2d");
  self.entities = {};
  self.changeWeatherTime = self.settings.game.changeWeatherTime;
  return self;
};

Foreground.prototype.init = function () {
  var self = this;
  self.gameId = requestAnimationFrame(function(time) { self.run(time); });
  self.settings.game.addGameCanvasLoop(self.gameId);
};

Foreground.prototype.run = function (time) {
  var self = this;

  self.clear();
  self.checkTime(time);
  
  for (var entityType in self.entities) {
    if (self.entities.hasOwnProperty(entityType)) {    
      self.entities[entityType].forEach(function (entity) {
        entity.move();
        entity.draw(self.cntx, time);
      });
    }
  }

  if (self.settings.game.ended) {
    self.endGame();
  } else {
    requestAnimationFrame(function(time) { self.run(time); });
  } 
};

Foreground.prototype.addEntity = function (entity, type) {
  type = type || "default";
  if (!this.entities[type]) this.entities[type] = [];
  this.entities[type].push(entity);
};

Foreground.prototype.checkTime = function (time) {
  var self = this;

  // Once every 3 minutes run this
  if (time > self.changeWeatherTime) {
    // change so the next season can occur
    self.changeWeatherTime += self.settings.game.changeWeatherTime;
    self.settings.game.PubSub.notify("Game:ChangeWeatherTime");
  }
};

Foreground.prototype.clear = function () {
  var self = this;
  self.cntx.clearRect(0, 0, self.settings.game.w, self.settings.game.h);
};

Foreground.prototype.endGame = function () { 
  var self = this;
  console.log("END FOREGROUND");
  self.cntx.fillStyle = "rgba(255,255,255,0.7)";
  self.cntx.fillRect(0, 0, self.settings.game.w, self.settings.game.h);
}; 
