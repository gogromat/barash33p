/* global getRandom */

var Sun = function (settings) {
    var self = this;
    self.settings = settings;

    self.x = getRandom(0, self.getSunImageWidth() - 100);
    self.y = getRandom(self.settings.maxHeight, self.settings.minHeight);
    // self.speed = 0.2;
    self.time = 0;
    self.minHeight = self.settings.minHeight;
    self.maxHeight = self.settings.maxHeight;
};

Sun.prototype.getSunRandomSpeed = function () {
return getRandom(0.2, 1.1);
};

Sun.prototype.getSunImageWidth = function () {
    return this.settings.canvas.w;
};

Sun.prototype.getClientWidth = function () {
    return this.settings.game.settings.w;
};

Sun.prototype.move = function (time) {
    var self = this,
        sunImageWidth = self.getSunImageWidth();
    if (time - self.time  >= 60) {
        self.time = time;
        self.x--;
    }
    if (self.x <= -(sunImageWidth)) {
        self.x = self.getClientWidth() + sunImageWidth;
    }
};

Sun.prototype.draw = function (context, time) {
    var self = this,
        position = self.settings.canvas;
    self.move(time);
    context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};