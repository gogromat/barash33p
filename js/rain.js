/* global _ */

var Rain = function (settings) {
    var self = this;
    self.setSettings(settings);
};

Rain.prototype.setSettings = function(settings) {
    var self = this;
    self.settings = settings;
    _.extend(self, settings);
};

Rain.prototype.getRainImageWidth = function () {
    return this.settings.canvas.w;
};

Rain.prototype.getClientWidth = function () {
    return this.settings.game.settings.w;
};

Rain.prototype.move = function () {
    var self = this;
    self.y++;
};

Rain.prototype.draw = function (context) {
	var self = this,
		position = self.settings.canvas;
	self.move();
	context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};