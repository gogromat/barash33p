/* global getRandom, async */

var Cloud = function (settings) {
    var self = this;
    self.settings = settings;
   
    self.minHeight = self.settings.minHeight;
    self.maxHeight = self.settings.maxHeight;

    self.x  = getRandom(0, self.getClientWidth() + self.getCloudImageWidth() );
    self.y  = self.getRandomHeight();
    self.speed = self.getCloudRandomSpeed();
    //self.direction = "DOWN";

    self.settings.game.PubSub.subscribe("Game:ChangeWeather", function (params) {
        self.changeWeather(params);
    });
};

Cloud.prototype.getRandomHeight = function () {
    var self = this;
    return getRandom(self.maxHeight, self.minHeight);
};

Cloud.prototype.getCloudRandomSpeed = function () {
    return getRandom(0.1, 0.2);
};

Cloud.prototype.getCloudImageWidth = function () {
    return this.settings.canvas.w;
};

Cloud.prototype.getClientWidth = function () {
	return this.settings.game.settings.w;
};

Cloud.prototype.changeWeather = function(params) {
    // console.log("cloud is changing weather", params);
    var self = this,
        currentTime = Date.now(),
        time = params.time,
        millisecondsToRepeatAction = time / 10,
        weather = params.weather;

    if (self.settings.canvases && self.settings.canvases[weather]) {
        self.settings.canvas = self.settings.canvases[weather];
    }

    async.forever(function (next) {

        setTimeout(function () {

            var date = Date.now();

            if (date - currentTime < time) {
                self.settings.game.PubSub.notify("Cloud:ChangeWeather", { weather: weather, cloud: self });
                next();
            } else {
                next("err!");
            }

        }, millisecondsToRepeatAction);
        
    }, function (err) {
    });

};

Cloud.prototype.move = function () {
	var self = this,
		thisCloudWidth = self.getCloudImageWidth();
    
    self.x -= self.speed;
    
    //if (self.direction == "DOWN") {
      // cloud goes down (positive direction)
      //self.y+= getRandom(0,1);
    //} else {
      // cloud goes up (negative direction)
      //self.y-= getRandom(0,1);
    //}
    
    if (self.x <= -(thisCloudWidth)) {
        self.x = self.getClientWidth() + thisCloudWidth;
        self.speed = self.getCloudRandomSpeed();
        self.minHeight = getRandom(self.settings.minHeight/2, self.settings.minHeight);
        self.maxHeight = getRandom(0, self.settings.maxHeight);
        self.y = self.getRandomHeight();
    }
    /*
    if (self.y >= self.minHeight) {
      self.direction = "UP";
    } else if (self.y <= self.maxHeight) {
      self.direction = "DOWN";
    }*/
};

Cloud.prototype.draw = function (context) {
	var self = this,
		position = self.settings.canvas;
	self.move();
	context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};