/* global DIRECTIONS */

var Character = function (settings) {
    var self = this;
    self.settings = settings;
    self.init();
};

Character.prototype.init = function () {
    var self = this;
    self.x = self.settings.x;
    self.y = self.settings.y;
    self.type = self.settings.type;
    self.onGround = self.settings.onGround;
};

Character.prototype.getCharacterImageWidth = function () {
    return this.settings.canvas.w;
};

Character.prototype.getClientWidth = function () {
    return this.settings.game.settings.w;
};

Character.prototype.getXSpeed = function () {
  // Returns the appropriate X character speed based on whether or not character is on the ground
  var self = this;
  return (self.onGround ? self.settings.XSpeed : self.settings.gravityXSpeed);
};

Character.prototype.moveUp = function () {
    var self = this;

    self.settings.game.PubSub.notify("Character:Move:Up");


    if (self.hasOwnProperty("leftUp")) {
      if (self.leftUp > 0) {
        // character goes up
        self.y -= self.settings.gravityYSpeed;
        self.leftUp--;
      }
      else if (self.leftUp === 0) {
        // character reached peak
        self.leftUp--;
        self.direction = "DOWN";
      }
    } else {
      // Start "gravity" sequence
      self.onGround = false;
      self.leftUp = self.settings.gravityTime || self.settings.game.dh;
      self.settings.game.PubSub.notify("Character:Jump", { time: self.leftUp });
    }
};

Character.prototype.moveLeft = function () {
  var self = this;
  
  // if character is trying to get outside of game boundaries
  // to the left side
  if (self.x <= 0) {
    // cannot fall out of game to the left
    self.x = 0;
  }

  self.x += DIRECTIONS[self.direction].x * self.getXSpeed();
};

Character.prototype.moveRight = function () {
  
  var self = this,
      shouldMoveRight = true,
      // Speed at which canvas should move
      characterXSpeed = self.getXSpeed(),
      rightBoundary = self.settings.game.w - self.settings.game.dw;
  
  // if character passed certain percentage of the screen, notify 
  // other canvases of movement to the right (and move them)
  if (self.x + self.settings.game.dw >= self.settings.game.w * (self.settings.percentAtWhichStageMoves || 0.25)) {
    self.settings.game.PubSub.notify("Character:Move:Right", { speed: characterXSpeed });
    shouldMoveRight = false;
  }
  
  // if character is trying to get outside of game boundaries
  // to the right side (should not happen, but just a precaution)
  if (self.x >= rightBoundary) {
    // cannot fall out of game to the right
    self.x = rightBoundary;
  }

  if (shouldMoveRight) {
    self.x += DIRECTIONS[self.direction].x * characterXSpeed;
  }
};

Character.prototype.move = function (time) {
	// Stage moves when character reaches certain % of it
	// and notifies all the dependent elements through PubSub
    // That's why we notify all interested parties in character movement

    var self = this;
    
    self.settings.game.PubSub.notify("Character:Move");
    // From notifying PubSub we set - this.blockBeneathIsLand and this.direction

    //console.log(self.direction, self.x, self.y);


    if (self.blockBeneathIsLand) {
        // console.log("On Land Block")
        self.onGround = true;
        self.isFalling = false;
        delete(self.leftUp);
    } else {
        self.onGround = false;
    }

    // console.clear();
    // console.log(self.direction, self.onGround,  self.y, self.y + self.settings.game.dh)

    if (self.y + self.settings.game.dh > self.settings.game.h) {
        // Character falls below floor, do not react on other commands
        if (self.y > self.settings.game.h) {
            // Character is fully under the floor, game ends
            self.settings.game.PubSub.notify("Character:Dead");
        } else {
            // Continue to fall down if character is not fully under canvas
            self.y += DIRECTIONS.DOWN.y * self.settings.gravityYSpeed;
        }
        return;
    }

    if (self.direction.indexOf("LEFT") > -1) {
        // if character is going LEFT[|UP|DOWN]
        self.moveLeft();  
    } else if (self.direction.indexOf("RIGHT") > -1) {
        // if character is going RIGHT[|UP|DOWN]
        self.moveRight();
    }

    if (self.direction.indexOf("UP") > -1) {
        // if character is going UP[|LEFT|RIGHT]
        if (!self.isFalling) {
            self.moveUp();
        }
    }

    if (!self.onGround) {
        // if character is not on the ground,
        // then character is going DOWN
        if (self.hasOwnProperty("leftUp")) {
            if (self.leftUp < 0) {
                // character goes down
                self.leftUp--;
                self.y += DIRECTIONS.DOWN.y * self.settings.gravityYSpeed;
            } else {
                // character goes up
                self.moveUp();
            }
        } else {
            self.isFalling = true;
            self.y += DIRECTIONS.DOWN.y * self.settings.gravityYSpeed;
        }
        //return false;
    }

};

Character.prototype.draw = function (context, time) {
    var self = this,
    position = self.settings.canvas;
    self.move(time);
    self.checkIsDead();
    context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};

// Checks whether or not character is dead
// For now death = fell out of canvas
Character.prototype.checkIsDead = function () {
  var self = this;
  if (self.y >= self.settings.game.h) {
    self.settings.game.PubSub.notify("Character:Dead");
  }
};