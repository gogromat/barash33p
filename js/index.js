/* global Game, Foreground, Background, SpriteCanvasCacher, Character, Block, Sun, Cloud, Rain, _, async, Key */


var getRandom = function (min, max) {
    //     guarantees whole  [0,1)          guar. max   guar. min 
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
var getPercentRandom = function (percent) {
    if (getRandom(1, 100) <= percent) return true;
    return false;
};

var DIRECTIONS = {
    RIGHT:     { x: 1, y: 0 }, RIGHTUP:   { x: 1, y:-1 }, RIGHTDOWN: { x: 1, y: 1 },
    LEFT:      { x:-1, y: 0 }, LEFTUP:    { x:-1, y:-1 }, LEFTDOWN:  { x:-1, y: 1 },
    DOWN:      { x: 0, y: 1 }, UP:        { x: 0, y:-1 }, CENTER:    { x: 0, y: 0 }
};


document.addEventListener('DOMContentLoaded', function () {
    
    var game = new Game({
        w: 1000, 
        h: 400, 
        dw: 40, 
        dh: 40,
        maxWaterBlocks: 3,
        // Change weather every minute
        // changeWeatherTime: (1000 * 60 * 1)
        changeWeatherTime: (10000), // 10 seconds,
        weather: {
            normal: {

            },
            rain: {

            },
            snow: {

            },
            drought: {

            },
            fog: {

            }
        },
        currentWeather: "normal"
    });

    var entrySprite = new Image();
    
    entrySprite.src = "images/spriteSheet.svg";
    
    
    var tree = new Image();
    tree.onload = function () {
        tree.tree = {
            x: 0, y: 0, w: game.dw, h: game.dh, type: "image"
        };
    };
    tree.src = "images/backgroundEntities.svg";
    

    var backgrounds = {
            normal  :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba(147,206,222,  1)", type: "color" },
            rain    :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba( 44, 92,128,220)", type: "color" },
            snow    :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba(  0, 60, 60,  1)", type: "color" },
            drought :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba(244,241, 77,  1)", type: "color" },
            fog     :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba( 83,107, 98,153)", type: "color" },
        },
        particles = { 
            rain:  { x:   0, y: 320, w:  5, h: 10, type: "image" },
            snow:  { x:   5, y: 320, w: 10, h: 10, type: "image" },
            snow2: { x:  15, y: 320, w: 10, h: 10, type: "image" },
        },
        clouds = {
            normal :   { x:   0, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            rain   :   { x:  40, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            snow   :   { x:  80, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            drought:   { x: 120, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            fog    :   { x: 160, y: 360, w:  game.dw, h:  game.dh, type: "image" },
        };

    
    entrySprite.sun   = { x:  40, y: 200, w:  game.dw, h:  game.dh, type: "image" };
    
    entrySprite.land1 = { x:   0, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.land2 = { x:  40, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.land3 = { x:  80, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.land4 = { x: 120, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    
    entrySprite.crate1 = { x:   0, y: 280, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.crate2 = { x:  40, y: 280, w:  game.dw, h:  game.dh, type: "image" };


    entrySprite.character = 
                        { x:   0, y:  80, w:  game.dw, h:  game.dh, type: "image" };

    entrySprite.waterBaseLevel1 = 
                        { x:   0, y: 160, w:  160, h:  game.dh, 
                          type: "pattern",
                          // width of a pattern, in order to be complete, has to
                          // have n steps. we force it to be n steps of canvas width
                          canvasW: 160 * Math.ceil(game.w / 160), 
                          direction: "repeat-x"
                        };
    entrySprite.waterBaseLevel2 = 
                        { x:   0, y: 120, w:  160, h:  game.dh, 
                          type: "pattern",
                          // width of a pattern, in order to be complete, has to
                          // have n steps. we force it to be n steps of canvas width
                          canvasW: 160 * Math.ceil(game.w / 160), 
                          direction: "repeat-x"
                        };

    entrySprite.water = { x:   0, y: 120, w:  game.dw, h:  game.dh, type: "image" };

    entrySprite.onload = function () {

        // Background canvases
        var backgroundCanvases = {};
        for (var type in backgrounds) {
            backgroundCanvases[type] = new SpriteCanvasCacher({
                sprite: entrySprite,
                position: backgrounds[type]
            });
        }

        // Cloud canvases
        var cloudCanvases = {};
        for (var cloudType in clouds) {
            cloudCanvases[cloudType] = new SpriteCanvasCacher({
                sprite: entrySprite,
                position: clouds[cloudType]
            });
        }

        // 
        var rainDropCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, 
            position: particles.rain
        });
        var snowCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, 
            position: particles.snow
        });
        var snowCanvas2 = new SpriteCanvasCacher({
            sprite: entrySprite, 
            position: particles.snow2
        });


        // Sun canvas
        var sunCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.sun
        });
        
        // Land Block canvases
        var landBlock1 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land1
        });
        var landBlock2 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land2
        });
        var landBlock3 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land3
        });
        var landBlock4 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land4
        });
        var landBlocksCanvases = [ landBlock1, landBlock2, landBlock3, landBlock4 ];

        // Crates
        var crateBlock1 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.crate1
        });
        var crateBlock2 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.crate2
        });
        
        // Trees
        var tree1 = new SpriteCanvasCacher({
            sprite: tree, position: tree.tree
        });
        



        var characterCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.character
        });
        var waterCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.water
        });
        var waterBaseCanvasLevel1 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.waterBaseLevel1
        });
        var waterBaseCanvasLevel2 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.waterBaseLevel2
        });


        // Foreground
        var foreground = new Foreground({
            foreground: "#foreground",
            characterImage: "http://bit.ly/1lA252j",
            game: game
        });

        // Background
        var background = new Background({
            background: "#background",
            landImage: "http://bit.ly/1gkZGR9",
            game: game,
            canvas: backgroundCanvases.normal, canvases: backgroundCanvases
        });


        var entities = [];
        
        // Base Block objects
        for (var i = 0; i < game.settings.w + game.settings.dw; i+= game.settings.dw) {
            
            var newBlock;
        
            if (i < Math.ceil(game.settings.w * 0.5) ) {
                // First 50% of screen are the land blocks
                newBlock = getLandBlock(i, game.getBaseBlockTop() );
            } else {
                // Next is random
                newBlock = getRandomBlock(i, game.getBaseBlockTop() );
            }
            
            entities.push({
                entity: new Block(newBlock),
                type: "baseBlocks"
            });
        }

        // Sun Object
        entities.push({
            entity: new Sun({   game : game, canvas:   sunCanvas, minHeight: 10, maxHeight: 0 }),
            type: "sun"
        });

        // Cloud Objects
        for (var c = 0; c < 20; c++) {
            entities.push({
                entity: new Cloud({ 
                    game : game, canvas: cloudCanvases.normal, 
                    minHeight: getRandom(15, 45), maxHeight: getRandom(0, 15), canvases: cloudCanvases
                }),
                type:"cloud"
            });
        }

        // Character Object
        entities.push({
            entity: new Character({
                game: game, 
                canvas: characterCanvas,
                x: game.settings.dw,
                // Above the base block
                y: game.getBaseBlockTop() - game.settings.dh,
                direction: "CENTER",
                onGround: true,
                XSpeed: 2,
                YSpeed: 2,
                gravityXSpeed: 2,
                gravityYSpeed: 1,
                gravityTime: 62,
                percentAtWhichStageMoves: 0.5
            }),
            type: "character"
        });

        // 2 second-level parallax water blocks that follow each other
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel2,
                game: game, 
                type: "baseWater",
                x: -40,
                y: game.getBaseBlockTop() + 12,
                level: 2
            }),
            type: "parallaxWater2",
        });
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel2,
                game: game, 
                type: "baseWater",
                x: -40 + waterBaseCanvasLevel2.canvasW,
                y: game.getBaseBlockTop() + 12,
                level: 2
            }),
            type: "parallaxWater2",
        });
        // 2 first-level parallax water blocks that follow each other
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel1,
                game: game, 
                type: "baseWater",
                x: 0,
                y: game.getBaseBlockTop() + 20,
                level: 1
            }),
            type: "parallaxWater",
        });
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel1,
                game: game, 
                type: "baseWater",
                x: waterBaseCanvasLevel1.canvasW,
                y: game.getBaseBlockTop() + 20,
                level: 1
            }),
            type: "parallaxWater",
        });




        function getLandBlock (x, y) {
            var randomBlockNum = getRandom(0, landBlocksCanvases.length-1);
            return {
                game: game, 
                canvas: landBlocksCanvases[randomBlockNum],
                type: "land",
                x: x, y: y
            };
        }

        function getWaterBlock (x, y, sequence) {
            return {
                game: game, 
                canvas: waterCanvas,
                type: "water",
                x: x,
                y: y,
                sequence: sequence || 1
            };
        }

        function getRandomBlock (x, y) {
            var baseBlocks = foreground.entities.baseBlocks || [],
                isLand = getPercentRandom(90),
                lastBlock = baseBlocks[baseBlocks.length - 1],
                sequence = 1;
            
            if (baseBlocks && lastBlock && lastBlock.type == "water") {
                isLand = getPercentRandom(40);
                sequence = lastBlock.settings.sequence + 1;
                if (sequence > game.settings.maxWaterBlocks) isLand = true;
            }

            if (isLand) {
                return getLandBlock(x, y);
            } else {
                return getWaterBlock(x, y, sequence);
            }
        }

        function getBlocksBeneathOtherBlock (otherBlock, blocks) {
            
                // Get block's left side
            var blockLeftSide = otherBlock.x,
                // Get block's right side
                blockRightSide = blockLeftSide + game.dw,
                // Get block's bottom
                blockBottom = otherBlock.y + game.dh,
                // Get the block difference from our block
                allBlocksLeftSide = blocks[0].x,
                remainderFromBlockWidth = (blockLeftSide ? blockLeftSide % game.dw : 0),
                blockDifference = allBlocksLeftSide > 0 ? 
                                    remainderFromBlockWidth - allBlocksLeftSide :
                                    remainderFromBlockWidth + Math.abs(allBlocksLeftSide),
                minimumBlockX,
                maximumBlockX;

                minimumBlockX = blockLeftSide - blockDifference;
                maximumBlockX = blockLeftSide + blockDifference;
                if (maximumBlockX < blockRightSide) maximumBlockX += game.dw;

                var filtered =  _.filter(blocks, function (b) {
                    return b.x >= minimumBlockX && b.x <= maximumBlockX && b.y == blockBottom;
                });

                return filtered;
        }
        
        function getHPercentOfTwoBlock (block1, block2) {
            return 1 - (Math.abs(block1.x - block2.x) / game.dw);
        }

        function checkBlocksBeneathCharacterAreLand (block, blocks) {
            
            var blocksBeneath = getBlocksBeneathOtherBlock(block, blocks),
                landType = ["land"],
                //thirdOfABlock = Math.floor(game.dw * 0.33),
                isLandOne = (blocksBeneath[0] && landType.indexOf(blocksBeneath[0].type) > -1 ),
                isLandTwo = (blocksBeneath[1] && landType.indexOf(blocksBeneath[1].type) > -1 ),
                percentOfCharacterOnBlockOne;
            
            // console.log(
            //     blocksBeneath[0] ? blocksBeneath[0].x : "", blocksBeneath[0] ? blocksBeneath[0].y : "",              
            //     blocksBeneath[1] ? blocksBeneath[1].x : "", blocksBeneath[1] ? blocksBeneath[1].y : "" );
            
            if (blocksBeneath.length <= 0) {
                // No blocks
                
                return false;
            } else if (blocksBeneath.length == 1) {
                // If character stands on one block
                //console.clear();
                //console.table("Only 1 block", getHPercentOfTwoBlock(block, blocksBeneath[0]), "<" );
                
                if (getHPercentOfTwoBlock(block, blocksBeneath[0]) > 0.33) {
                    return isLandOne;
                }
                return false;
            } else {
                
                //console.clear();
                //console.table(blocksBeneath);
                // If character stands on multiple blocks

                // If both blocks are land
                if (isLandOne && isLandTwo) return true;

                if (getHPercentOfTwoBlock(block, blocksBeneath[0]) > 0.33) {
                    return isLandOne;
                } else {
                    return isLandTwo;
                }
            }
            return false;
        }

        function moveBlocks(blockEntities, callback) {
            if (blockEntities && blockEntities.length) {            
                var newTopBlock,
                    oneBlockWidth = blockEntities[0].settings.canvas.canvasW || 
                                    blockEntities[0].settings.canvas.w,
                    lastBlockRightSide;

                // entity is out of canvas
                if (blockEntities[0].x + oneBlockWidth <= 0) {

                    // Remove the first block
                    newTopBlock = blockEntities.shift();

                    // Get the last block's right side
                    lastBlockRightSide = blockEntities[blockEntities.length - 1].x + oneBlockWidth;
                                    
                    // Place the removed block to the right side
                    newTopBlock.x = lastBlockRightSide;

                    // if needed, setup block settings
                    if (callback) newTopBlock = callback(newTopBlock, blockEntities, lastBlockRightSide);

                    // push offset block to the back of block entities array
                    blockEntities.push(newTopBlock);
                }
            }
        }

        function removeBlocksOutOfCanvas(blockEntities) {
            if (blockEntities && blockEntities.length) {
                console.clear();
                // console.log(blockEntities);
                var oneBlockWidth = blockEntities[0].settings.canvas.canvasW || 
                    blockEntities[0].settings.canvas.w;

                if (blockEntities[0].x + oneBlockWidth <= 0) {
                    // Remove the block
                    blockEntities.shift();
                }
            }
        }

        function setCharacterDirectionFromPressedKeys() {
            // Setup character's current direction based on keys that are pressed
            var character = foreground.entities.character[0];
            if (!_.some(Key.pressed)) {
                // keep previous direction, or go down
                character.direction = "DOWN";
            } else {
                character.direction = "";
                     if (Key.isDown(Key.RIGHT)) character.direction  = "RIGHT";
                else if (Key.isDown(Key.LEFT))  character.direction  = "LEFT";
                     if (Key.isDown(Key.UP))    character.direction += "UP";
                else if (Key.isDown(Key.DOWN))  character.direction += "DOWN";
            }
        }


        // Character moved. 
        // Check if block beneath is a land block.
        // Setup character direction from currently pressed keys
        game.PubSub.subscribe("Character:Move", function () {
            var character = foreground.entities.character[0],
                // for now, add more types later
                allBlocks = foreground.entities.baseBlocks.concat(foreground.entities.crates ? foreground.entities.crates : []);
            
            character.blockBeneathIsLand = checkBlocksBeneathCharacterAreLand(character, allBlocks);
            setCharacterDirectionFromPressedKeys();
        });

        // Character moved to the right.
        // Start moving the blocks, water and other moving elements
        game.PubSub.subscribe("Character:Move:Right", function () {

            moveBlocks(
                foreground.entities.baseBlocks,
                function (newBlock, blockEntities, lastBlockRightSide) {
                    var newBlockSettings = getRandomBlock(lastBlockRightSide, newBlock.y);
                    newBlock.setSettings(newBlockSettings);
                    return newBlock;
                }
            );

            moveBlocks(foreground.entities.parallaxWater);
            moveBlocks(foreground.entities.parallaxWater2);


            var baseBlocks = foreground.entities.baseBlocks,
                lastBaseBlock = baseBlocks[baseBlocks.length - 1],
                duh = lastBaseBlock.x % game.dw,
                shouldAddCrate = getPercentRandom(5),
                shouldAddTree = getPercentRandom(8);
                

            if (!duh && lastBaseBlock.type == "land" && shouldAddCrate) {                
                foreground.addEntity(
                    new Block({
                        x: lastBaseBlock.x,
                        y: lastBaseBlock.y - game.dh,
                        game: game,
                        canvas: (getRandom(0, 1) ? crateBlock1 : crateBlock2),
                        type: "land"
                    }),
                    "crates"
                );
            } else if (!duh && lastBaseBlock.type == "land" && shouldAddTree) {
                foreground.addEntity(
                    new Block({
                        x: lastBaseBlock.x,
                        y: lastBaseBlock.y - game.dh,
                        game: game,
                        canvas: tree1,
                        type: "land"
                    }),
                    "trees"
                );                
            }

            removeBlocksOutOfCanvas(foreground.entities.crates);
            removeBlocksOutOfCanvas(foreground.entities.trees);

        });


        // It is time to change the weather!
        game.PubSub.subscribe("Game:ChangeWeatherTime", function () {

            var allWeather = _.keys(game.weather),
                // figure out what meteorologists say the weather will be like
                newWeather = allWeather[getRandom(0, allWeather.length-1)];
            
            // console.log("changing weather!", newWeather);

            game.PubSub.notify("Game:ChangeWeather", { 
                weather: newWeather,
                time: game.changeWeatherTime
            });

        });

        // Cloud Changes Weatehr
        game.PubSub.subscribe("Cloud:ChangeWeather", function (object) {
            
            var cloud = object.cloud,
                x = cloud.x,
                y = cloud.y + (game.dh / 2),
                thirdBlockWidth = game.dw / 3,
                weather = object.weather;

            // console.log("adding rain drops");

            if (weather == "rain") {            
                
                // spawn 3 rain particles
                for (var i = 0; i < 3; i++) {
                    
                    foreground.addEntity(new Rain({
                        x: x + (thirdBlockWidth * i),
                        // right from the middle of the cloud
                        y: y,
                        canvas: rainDropCanvas
                    }));
                }

            } else if (weather == "snow") {

                // spawn 2 snow particles
                for (var j = 0; j < 2; j++) {
                    
                    foreground.addEntity(new Rain({
                        x: x + (thirdBlockWidth * j),
                        // right from the middle of the cloud
                        y: y,
                        canvas: (getRandom(0, 1) ? snowCanvas : snowCanvas2)
                    }));
                }

            }


        });





        // Adds base blocks to the foreground
        // Adds sun to the foreground
        // Adds a bunch of clouds to the foreground
        // Adds character to the foreground
        // Adds 2 level 2 parallax waves
        // Adds 2 level 1 parallax waves
        entities.forEach(function (entity) {
            foreground.addEntity(entity.entity, entity.type);
        });

        // Add canvases to game
        game.addStage(background)
            .addStage(foreground)
            .startGame();

        game.PubSub.subscribe("Character:Dead", function () {
            game.endGame();
        });




        document.querySelector(".newGame").onclick = function () {
            game.endGame();
            console.log("new game");
            game.startGame();
        };

        // Keyboard keys pressed
        document.onkeydown = document.onkeyup = function (event) { 
            Key.onKeyDown(event);
        };

        console.clear();
        console.log("New Game");
    };

});