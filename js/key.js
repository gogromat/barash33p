var Key = {
    pressed : [],
    LEFT  : 37,
    RIGHT : 39,
    DOWN  : 40,
    UP    : 38,
	isDown : function (keycode) {
        return this.pressed[keycode];
	},
	onKeyDown : function (e) {
        this.pressed[e.keyCode] = e.type == 'keydown';  
	}
};
