/* global requestAnimationFrame */

var Background = function (settings) {
    var self = this;
    self.settings = settings;
    self.background = document.querySelector(self.settings.background);
    self.cntx = self.background.getContext("2d");
    self.entities = [];

    self.settings.game.PubSub.subscribe("Game:ChangeWeather", function (params) {
        var weather = params.weather;
        // console.log("Background change", params, weather, self.settings.canvases);
        if (self.settings.canvases && self.settings.canvases[weather]) {
            self.settings.canvas = self.settings.canvases[weather];
            self.draw();
        }
    });

    return self;
};

Background.prototype.init =  function (timestamp) {
  var self = this;
  self.gameId = requestAnimationFrame(function (time) { self.run(time); });
  self.settings.game.addGameCanvasLoop(self.gameId); 
};

Background.prototype.run =  function (timestamp) {
  var self = this;
  self.timestamp = timestamp;
  self.draw(); 
  self.drawBorders();

  self.entities.forEach(function (entity) {
    entity.move();
    entity.draw(self.cntx);
  });
};

Background.prototype.addEntity = function (entity) {
  this.entities.push(entity);
};

Background.prototype.draw = function () {
  var self = this,
    position = self.settings.canvas;
    //console.log(self.settings.canvas.canvas, position.x, position.y, position.w, position.h)
  self.cntx.drawImage(self.settings.canvas.canvas, position.x, position.y, position.w, position.h);
};

Background.prototype.drawBorders = function () {
  var self = this;
  for (var width = 0; width <= self.settings.game.w; width += self.settings.game.dw) {
    for (var height = 0; height <= self.settings.game.h; height += self.settings.game.dh) {
      self.cntx.strokeStyle = "green";
      self.cntx.strokeRect(width, height, self.settings.game.dw, self.settings.game.dh);
      
      self.cntx.textBaseline = "hanging";
      self.cntx.fillStyle = "black";
      self.cntx.font = "bold 8px sans-serif";
      self.cntx.fillText("x:" + width + "y:" + height, width, height);
    }
  }
};