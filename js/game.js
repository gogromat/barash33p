/* global PubSub, _ */

var Game = function (settings) {
    this.gameLoops = [];
    this.stages = [];
    this.ended = false;
    this.PubSub = PubSub;
    this.setSettings(settings);
    return this;
};

Game.prototype.setSettings = function (settings) {
    var self = this;
    self.settings = settings;
    _.extend(self, settings);
};


Game.prototype.addGameCanvasLoop = function (loopId) {
    this.gameLoops.push(loopId);
    return this;
};

Game.prototype.getBaseBlockTop = function () {
    // returns the top position of base blocks
    return this.h - this.dh;
};


Game.prototype.addStage = function (stage) {
    this.stages.push(stage);
    return this;
};

Game.prototype.startGame = function () {
    var self = this;

    self.ended = false;

    // timeout to load images,
    // todo: move images outside and "feed" them into background/foreground as objects
    setTimeout(function() {
        self.stages.forEach(function (stage) {
            stage.init();
        });
    }, 1000);
    return self;
};

Game.prototype.endGame = function () {
    // stop all canvases redraw call
    this.ended = true;
    this.gameLoops.forEach(function(loopId) {
        window.cancelAnimationFrame(loopId);
    });
    // clear game loops
    this.gameLoops = [];
    // but keep stages
};