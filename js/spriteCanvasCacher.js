/* global _ */

(function (_) {

    var root = this,
        spritesCached = [];

    var SpriteCanvasCacher = function (settings) {

        var self = this,
            foundSprite,
            foundPosition;

        spritesCached.forEach(function (sc) {
            if (sc.sprite == settings.sprite) {
                foundSprite = sc;
                foundPosition = _.find(sc.positions, settings.position);
            }
        });

        if (!foundSprite) {

            // console.log("NOT FOUND");

            // Sprite not found, add new one

            foundPosition = self.setNewPosition(settings.position);
            self.tryDrawing(settings.sprite, foundPosition);
        
            // cache the sprite and this first position, with it's canvas
            spritesCached.push({
                sprite: settings.sprite,
                positions: [foundPosition]
            });

        } else {
            // Sprite Found

            if (!foundPosition) {
                // Position Not Found, add new one
                foundPosition = self.setNewPosition(settings.position);
                self.tryDrawing(foundSprite.sprite, foundPosition);
                foundSprite.positions.push(foundPosition);
            } 
       }

        return foundPosition;
    };


    SpriteCanvasCacher.prototype.setCanvasContext = function (position) {
        var canvas    = document.createElement("canvas"),
            context   = canvas.getContext("2d");
        canvas.width  = position.canvasW || position.w;
        canvas.height = position.canvasH || position.h;
        return { canvas: canvas, context: context };
    };

    SpriteCanvasCacher.prototype.setNewPosition = function (position) {
        var self = this,
            canvasContext = self.setCanvasContext(position);
        position.canvas   = canvasContext.canvas;
        position.context  = canvasContext.context;
        return position;
    };

    SpriteCanvasCacher.prototype.tryDrawing = function (sprite, position) {
        var self = this;

        if (position.type == "image") {
            // console.log("drawing image on a canvas");
            // console.log("Sprite",sprite);
            // Draw an image on a canvas
            position.context.drawImage(
                sprite,
                position.x, position.y, 
                position.w, position.h,
                // on the canvas at 0, 0 for w, h
                0, 0,
                position.w, position.h);
        
        } else if (position.type == "pattern") {

            console.log("here", sprite.width, sprite.height);

            // 1. draw image at x, y for w, h
            position.context.drawImage(
                sprite,
                // clip w, h at x, y
                position.x, position.y, 
                position.w, position.h,
                // on the canvas at 0, 0 for w, h
                0, 0,
                position.w, position.h);

            // 2. Obtain new image data at x, y for w, h
            var image = position.context.getImageData(
                0, 0, 
                position.w, position.h
            );

            // 3. Make image canvas
            var imageCanvas = self.setCanvasContext({w: position.w, h: position.h});

            // 4. Put image onto image canvas at 0 0      
            imageCanvas.context.putImageData(image, 0, 0);

            // 5. Clear canvas 
            position.context.clearRect(0, 0, 
                position.clientW || position.w,
                position.clientH || position.h
                );

            // 6. Create pattern element from image canvas
            position.pattern = position.context.createPattern(
                imageCanvas.canvas, 
                position.direction || "repeat"
            );

            position.context.fillStyle = position.pattern;
            
            position.context.fillRect(0, 0, 
                position.canvasW || position.w, 
                position.canvasH || position.h
            );
            //position.context.fill();

            position.w = position.canvasW;

        } else if (position.type == "color") {
            // console.log(position.color,position.x, position.y, position.w, position.h)
            position.context.fillStyle = position.color;
            position.context.fillRect(position.x, position.y, position.w, position.h);
        }
    };

    root.SpriteCanvasCacher = SpriteCanvasCacher;

}).call(this, _);