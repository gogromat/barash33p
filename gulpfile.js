var gulp = require('gulp'),
	// Plugins
	sass = require('gulp-sass'),
    livereload = require('gulp-livereload'),
    watch = require('gulp-watch'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	jshint = require('gulp-jshint'),
	rename = require('gulp-rename');

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('js/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

// Watch Files For Changes
gulp.task('watch', function () {
	var server = livereload();

	gulp.watch('js/*.js',		['lint', 'scripts']);
	gulp.watch('scss/*.scss',	['sass']);

	gulp.watch([
		'dist/*.js',
		'images/*.*',
		'css/*.css'
		]).on('change', function (file) {
		server.changed(file.path);
	});

})

gulp.task('default', [
	'lint', 'sass', 'scripts', 'watch'
	]);