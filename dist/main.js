/* global requestAnimationFrame */

var Background = function (settings) {
    var self = this;
    self.settings = settings;
    self.background = document.querySelector(self.settings.background);
    self.cntx = self.background.getContext("2d");
    self.entities = [];

    self.settings.game.PubSub.subscribe("Game:ChangeWeather", function (params) {
        var weather = params.weather;
        // console.log("Background change", params, weather, self.settings.canvases);
        if (self.settings.canvases && self.settings.canvases[weather]) {
            self.settings.canvas = self.settings.canvases[weather];
            self.draw();
        }
    });

    return self;
};

Background.prototype.init =  function (timestamp) {
  var self = this;
  self.gameId = requestAnimationFrame(function (time) { self.run(time); });
  self.settings.game.addGameCanvasLoop(self.gameId); 
};

Background.prototype.run =  function (timestamp) {
  var self = this;
  self.timestamp = timestamp;
  self.draw(); 
  self.drawBorders();

  self.entities.forEach(function (entity) {
    entity.move();
    entity.draw(self.cntx);
  });
};

Background.prototype.addEntity = function (entity) {
  this.entities.push(entity);
};

Background.prototype.draw = function () {
  var self = this,
    position = self.settings.canvas;
    //console.log(self.settings.canvas.canvas, position.x, position.y, position.w, position.h)
  self.cntx.drawImage(self.settings.canvas.canvas, position.x, position.y, position.w, position.h);
};

Background.prototype.drawBorders = function () {
  var self = this;
  for (var width = 0; width <= self.settings.game.w; width += self.settings.game.dw) {
    for (var height = 0; height <= self.settings.game.h; height += self.settings.game.dh) {
      self.cntx.strokeStyle = "green";
      self.cntx.strokeRect(width, height, self.settings.game.dw, self.settings.game.dh);
      
      self.cntx.textBaseline = "hanging";
      self.cntx.fillStyle = "black";
      self.cntx.font = "bold 8px sans-serif";
      self.cntx.fillText("x:" + width + "y:" + height, width, height);
    }
  }
};

/* global _ */

(function (_) {

    var root = this,
    
    Block = function (settings) {
        var self = this;
        self.setSettings(settings);
    
        self.settings.game.PubSub.subscribe("Character:Move:Right", function (params) {
            self.move(null, params);
        });
        if (self.type == "baseWater") {
            self.settings.game.PubSub.subscribe("Character:Jump", function (params) {
                self.leftUp = self.time = parseInt(params.time / 4, 10);
                console.log("w00t", self.leftUp, params.time);
            });
        }
    };
    
    Block.prototype.setSettings = function (settings) {
        var self = this;
        self.settings = settings;
        _.extend(self, settings);
    };
    
    Block.prototype.getBlockImageWidth = function () {
        return this.settings.canvas.w;
    };
    
    Block.prototype.getClientWidth = function () {
        return this.settings.game.settings.w;
    };
    
    Block.prototype.moveUp = function () {
        var self = this;
        self.time--;
        if (self.time == -self.leftUp) {
            delete(self.leftUp);
            return;
        }
        self.y = (self.time > 0 ? self.y - 1 : self.y + 1);
    };
    
    Block.prototype.move = function (time, params) {
        // block only moves when character reaches certain % of the screen
        // and notifies all the dependent elements through PubSub
    
        var self = this;
    
        /*
        if (self.type == "baseWater") {    
            if (self.hasOwnProperty("leftUp")) {
                self.moveUp();
            }
        }*/
    
    
        if (params) {
    
            if (self.type == "baseWater") {
                if (self.settings.level == 1) {
                    self.x -= params.speed * 0.44;
                } else if (self.settings.level == 2) {
                    self.x -= params.speed * 0.22;
                }            
            } else {
                self.x -= params.speed;
            }
        }
    
    };
    
    Block.prototype.draw = function (context, time) {
        var self = this,
            position = self.settings.canvas;
        self.move(time);
        context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
    };

    root.Block = Block;
    
}).call(this, _);
/* global DIRECTIONS */

var Character = function (settings) {
    var self = this;
    self.settings = settings;
    self.init();
};

Character.prototype.init = function () {
    var self = this;
    self.x = self.settings.x;
    self.y = self.settings.y;
    self.type = self.settings.type;
    self.onGround = self.settings.onGround;
};

Character.prototype.getCharacterImageWidth = function () {
    return this.settings.canvas.w;
};

Character.prototype.getClientWidth = function () {
    return this.settings.game.settings.w;
};

Character.prototype.getXSpeed = function () {
  // Returns the appropriate X character speed based on whether or not character is on the ground
  var self = this;
  return (self.onGround ? self.settings.XSpeed : self.settings.gravityXSpeed);
};

Character.prototype.moveUp = function () {
    var self = this;

    self.settings.game.PubSub.notify("Character:Move:Up");


    if (self.hasOwnProperty("leftUp")) {
      if (self.leftUp > 0) {
        // character goes up
        self.y -= self.settings.gravityYSpeed;
        self.leftUp--;
      }
      else if (self.leftUp === 0) {
        // character reached peak
        self.leftUp--;
        self.direction = "DOWN";
      }
    } else {
      // Start "gravity" sequence
      self.onGround = false;
      self.leftUp = self.settings.gravityTime || self.settings.game.dh;
      self.settings.game.PubSub.notify("Character:Jump", { time: self.leftUp });
    }
};

Character.prototype.moveLeft = function () {
  var self = this;
  
  // if character is trying to get outside of game boundaries
  // to the left side
  if (self.x <= 0) {
    // cannot fall out of game to the left
    self.x = 0;
  }

  self.x += DIRECTIONS[self.direction].x * self.getXSpeed();
};

Character.prototype.moveRight = function () {
  
  var self = this,
      shouldMoveRight = true,
      // Speed at which canvas should move
      characterXSpeed = self.getXSpeed(),
      rightBoundary = self.settings.game.w - self.settings.game.dw;
  
  // if character passed certain percentage of the screen, notify 
  // other canvases of movement to the right (and move them)
  if (self.x + self.settings.game.dw >= self.settings.game.w * (self.settings.percentAtWhichStageMoves || 0.25)) {
    self.settings.game.PubSub.notify("Character:Move:Right", { speed: characterXSpeed });
    shouldMoveRight = false;
  }
  
  // if character is trying to get outside of game boundaries
  // to the right side (should not happen, but just a precaution)
  if (self.x >= rightBoundary) {
    // cannot fall out of game to the right
    self.x = rightBoundary;
  }

  if (shouldMoveRight) {
    self.x += DIRECTIONS[self.direction].x * characterXSpeed;
  }
};

Character.prototype.move = function (time) {
	// Stage moves when character reaches certain % of it
	// and notifies all the dependent elements through PubSub
    // That's why we notify all interested parties in character movement

    var self = this;
    
    self.settings.game.PubSub.notify("Character:Move");
    // From notifying PubSub we set - this.blockBeneathIsLand and this.direction

    //console.log(self.direction, self.x, self.y);


    if (self.blockBeneathIsLand) {
        // console.log("On Land Block")
        self.onGround = true;
        self.isFalling = false;
        delete(self.leftUp);
    } else {
        self.onGround = false;
    }

    // console.clear();
    // console.log(self.direction, self.onGround,  self.y, self.y + self.settings.game.dh)

    if (self.y + self.settings.game.dh > self.settings.game.h) {
        // Character falls below floor, do not react on other commands
        if (self.y > self.settings.game.h) {
            // Character is fully under the floor, game ends
            self.settings.game.PubSub.notify("Character:Dead");
        } else {
            // Continue to fall down if character is not fully under canvas
            self.y += DIRECTIONS.DOWN.y * self.settings.gravityYSpeed;
        }
        return;
    }

    if (self.direction.indexOf("LEFT") > -1) {
        // if character is going LEFT[|UP|DOWN]
        self.moveLeft();  
    } else if (self.direction.indexOf("RIGHT") > -1) {
        // if character is going RIGHT[|UP|DOWN]
        self.moveRight();
    }

    if (self.direction.indexOf("UP") > -1) {
        // if character is going UP[|LEFT|RIGHT]
        if (!self.isFalling) {
            self.moveUp();
        }
    }

    if (!self.onGround) {
        // if character is not on the ground,
        // then character is going DOWN
        if (self.hasOwnProperty("leftUp")) {
            if (self.leftUp < 0) {
                // character goes down
                self.leftUp--;
                self.y += DIRECTIONS.DOWN.y * self.settings.gravityYSpeed;
            } else {
                // character goes up
                self.moveUp();
            }
        } else {
            self.isFalling = true;
            self.y += DIRECTIONS.DOWN.y * self.settings.gravityYSpeed;
        }
        //return false;
    }

};

Character.prototype.draw = function (context, time) {
    var self = this,
    position = self.settings.canvas;
    self.move(time);
    self.checkIsDead();
    context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};

// Checks whether or not character is dead
// For now death = fell out of canvas
Character.prototype.checkIsDead = function () {
  var self = this;
  if (self.y >= self.settings.game.h) {
    self.settings.game.PubSub.notify("Character:Dead");
  }
};
/* global getRandom, async */

var Cloud = function (settings) {
    var self = this;
    self.settings = settings;
   
    self.minHeight = self.settings.minHeight;
    self.maxHeight = self.settings.maxHeight;

    self.x  = getRandom(0, self.getClientWidth() + self.getCloudImageWidth() );
    self.y  = self.getRandomHeight();
    self.speed = self.getCloudRandomSpeed();
    //self.direction = "DOWN";

    self.settings.game.PubSub.subscribe("Game:ChangeWeather", function (params) {
        self.changeWeather(params);
    });
};

Cloud.prototype.getRandomHeight = function () {
    var self = this;
    return getRandom(self.maxHeight, self.minHeight);
};

Cloud.prototype.getCloudRandomSpeed = function () {
    return getRandom(0.1, 0.2);
};

Cloud.prototype.getCloudImageWidth = function () {
    return this.settings.canvas.w;
};

Cloud.prototype.getClientWidth = function () {
	return this.settings.game.settings.w;
};

Cloud.prototype.changeWeather = function(params) {
    // console.log("cloud is changing weather", params);
    var self = this,
        currentTime = Date.now(),
        time = params.time,
        millisecondsToRepeatAction = time / 10,
        weather = params.weather;

    if (self.settings.canvases && self.settings.canvases[weather]) {
        self.settings.canvas = self.settings.canvases[weather];
    }

    async.forever(function (next) {

        setTimeout(function () {

            var date = Date.now();

            if (date - currentTime < time) {
                self.settings.game.PubSub.notify("Cloud:ChangeWeather", { weather: weather, cloud: self });
                next();
            } else {
                next("err!");
            }

        }, millisecondsToRepeatAction);
        
    }, function (err) {
    });

};

Cloud.prototype.move = function () {
	var self = this,
		thisCloudWidth = self.getCloudImageWidth();
    
    self.x -= self.speed;
    
    //if (self.direction == "DOWN") {
      // cloud goes down (positive direction)
      //self.y+= getRandom(0,1);
    //} else {
      // cloud goes up (negative direction)
      //self.y-= getRandom(0,1);
    //}
    
    if (self.x <= -(thisCloudWidth)) {
        self.x = self.getClientWidth() + thisCloudWidth;
        self.speed = self.getCloudRandomSpeed();
        self.minHeight = getRandom(self.settings.minHeight/2, self.settings.minHeight);
        self.maxHeight = getRandom(0, self.settings.maxHeight);
        self.y = self.getRandomHeight();
    }
    /*
    if (self.y >= self.minHeight) {
      self.direction = "UP";
    } else if (self.y <= self.maxHeight) {
      self.direction = "DOWN";
    }*/
};

Cloud.prototype.draw = function (context) {
	var self = this,
		position = self.settings.canvas;
	self.move();
	context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};
/* global requestAnimationFrame */
/* jshint loopfunc: true */

var Foreground = function (settings) {
  var self = this;
  self.settings = settings;
  self.foreground = document.querySelector(self.settings.foreground);
  self.cntx = self.foreground.getContext("2d");
  self.entities = {};
  self.changeWeatherTime = self.settings.game.changeWeatherTime;
  return self;
};

Foreground.prototype.init = function () {
  var self = this;
  self.gameId = requestAnimationFrame(function(time) { self.run(time); });
  self.settings.game.addGameCanvasLoop(self.gameId);
};

Foreground.prototype.run = function (time) {
  var self = this;

  self.clear();
  self.checkTime(time);
  
  for (var entityType in self.entities) {
    if (self.entities.hasOwnProperty(entityType)) {    
      self.entities[entityType].forEach(function (entity) {
        entity.move();
        entity.draw(self.cntx, time);
      });
    }
  }

  if (self.settings.game.ended) {
    self.endGame();
  } else {
    requestAnimationFrame(function(time) { self.run(time); });
  } 
};

Foreground.prototype.addEntity = function (entity, type) {
  type = type || "default";
  if (!this.entities[type]) this.entities[type] = [];
  this.entities[type].push(entity);
};

Foreground.prototype.checkTime = function (time) {
  var self = this;

  // Once every 3 minutes run this
  if (time > self.changeWeatherTime) {
    // change so the next season can occur
    self.changeWeatherTime += self.settings.game.changeWeatherTime;
    self.settings.game.PubSub.notify("Game:ChangeWeatherTime");
  }
};

Foreground.prototype.clear = function () {
  var self = this;
  self.cntx.clearRect(0, 0, self.settings.game.w, self.settings.game.h);
};

Foreground.prototype.endGame = function () { 
  var self = this;
  console.log("END FOREGROUND");
  self.cntx.fillStyle = "rgba(255,255,255,0.7)";
  self.cntx.fillRect(0, 0, self.settings.game.w, self.settings.game.h);
}; 

/* global PubSub, _ */

var Game = function (settings) {
    this.gameLoops = [];
    this.stages = [];
    this.ended = false;
    this.PubSub = PubSub;
    this.setSettings(settings);
    return this;
};

Game.prototype.setSettings = function (settings) {
    var self = this;
    self.settings = settings;
    _.extend(self, settings);
};


Game.prototype.addGameCanvasLoop = function (loopId) {
    this.gameLoops.push(loopId);
    return this;
};

Game.prototype.getBaseBlockTop = function () {
    // returns the top position of base blocks
    return this.h - this.dh;
};


Game.prototype.addStage = function (stage) {
    this.stages.push(stage);
    return this;
};

Game.prototype.startGame = function () {
    var self = this;

    self.ended = false;

    // timeout to load images,
    // todo: move images outside and "feed" them into background/foreground as objects
    setTimeout(function() {
        self.stages.forEach(function (stage) {
            stage.init();
        });
    }, 1000);
    return self;
};

Game.prototype.endGame = function () {
    // stop all canvases redraw call
    this.ended = true;
    this.gameLoops.forEach(function(loopId) {
        window.cancelAnimationFrame(loopId);
    });
    // clear game loops
    this.gameLoops = [];
    // but keep stages
};
/* global Game, Foreground, Background, SpriteCanvasCacher, Character, Block, Sun, Cloud, Rain, _, async, Key */


var getRandom = function (min, max) {
    //     guarantees whole  [0,1)          guar. max   guar. min 
    return Math.floor(Math.random() * (max - min + 1)) + min;
};
var getPercentRandom = function (percent) {
    if (getRandom(1, 100) <= percent) return true;
    return false;
};

var DIRECTIONS = {
    RIGHT:     { x: 1, y: 0 }, RIGHTUP:   { x: 1, y:-1 }, RIGHTDOWN: { x: 1, y: 1 },
    LEFT:      { x:-1, y: 0 }, LEFTUP:    { x:-1, y:-1 }, LEFTDOWN:  { x:-1, y: 1 },
    DOWN:      { x: 0, y: 1 }, UP:        { x: 0, y:-1 }, CENTER:    { x: 0, y: 0 }
};


document.addEventListener('DOMContentLoaded', function () {
    
    var game = new Game({
        w: 1000, 
        h: 400, 
        dw: 40, 
        dh: 40,
        maxWaterBlocks: 3,
        // Change weather every minute
        // changeWeatherTime: (1000 * 60 * 1)
        changeWeatherTime: (10000), // 10 seconds,
        weather: {
            normal: {

            },
            rain: {

            },
            snow: {

            },
            drought: {

            },
            fog: {

            }
        },
        currentWeather: "normal"
    });

    var entrySprite = new Image();
    
    entrySprite.src = "images/spriteSheet.svg";
    
    
    var tree = new Image();
    tree.onload = function () {
        tree.tree = {
            x: 0, y: 0, w: game.dw, h: game.dh, type: "image"
        };
    };
    tree.src = "images/backgroundEntities.svg";
    

    var backgrounds = {
            normal  :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba(147,206,222,  1)", type: "color" },
            rain    :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba( 44, 92,128,220)", type: "color" },
            snow    :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba(  0, 60, 60,  1)", type: "color" },
            drought :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba(244,241, 77,  1)", type: "color" },
            fog     :  { x:   0, y:   0, w: game.w, h: game.h, color: "rgba( 83,107, 98,153)", type: "color" },
        },
        particles = { 
            rain:  { x:   0, y: 320, w:  5, h: 10, type: "image" },
            snow:  { x:   5, y: 320, w: 10, h: 10, type: "image" },
            snow2: { x:  15, y: 320, w: 10, h: 10, type: "image" },
        },
        clouds = {
            normal :   { x:   0, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            rain   :   { x:  40, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            snow   :   { x:  80, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            drought:   { x: 120, y: 360, w:  game.dw, h:  game.dh, type: "image" },
            fog    :   { x: 160, y: 360, w:  game.dw, h:  game.dh, type: "image" },
        };

    
    entrySprite.sun   = { x:  40, y: 200, w:  game.dw, h:  game.dh, type: "image" };
    
    entrySprite.land1 = { x:   0, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.land2 = { x:  40, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.land3 = { x:  80, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.land4 = { x: 120, y:  40, w:  game.dw, h:  game.dh, type: "image" };
    
    entrySprite.crate1 = { x:   0, y: 280, w:  game.dw, h:  game.dh, type: "image" };
    entrySprite.crate2 = { x:  40, y: 280, w:  game.dw, h:  game.dh, type: "image" };


    entrySprite.character = 
                        { x:   0, y:  80, w:  game.dw, h:  game.dh, type: "image" };

    entrySprite.waterBaseLevel1 = 
                        { x:   0, y: 160, w:  160, h:  game.dh, 
                          type: "pattern",
                          // width of a pattern, in order to be complete, has to
                          // have n steps. we force it to be n steps of canvas width
                          canvasW: 160 * Math.ceil(game.w / 160), 
                          direction: "repeat-x"
                        };
    entrySprite.waterBaseLevel2 = 
                        { x:   0, y: 120, w:  160, h:  game.dh, 
                          type: "pattern",
                          // width of a pattern, in order to be complete, has to
                          // have n steps. we force it to be n steps of canvas width
                          canvasW: 160 * Math.ceil(game.w / 160), 
                          direction: "repeat-x"
                        };

    entrySprite.water = { x:   0, y: 120, w:  game.dw, h:  game.dh, type: "image" };

    entrySprite.onload = function () {

        // Background canvases
        var backgroundCanvases = {};
        for (var type in backgrounds) {
            backgroundCanvases[type] = new SpriteCanvasCacher({
                sprite: entrySprite,
                position: backgrounds[type]
            });
        }

        // Cloud canvases
        var cloudCanvases = {};
        for (var cloudType in clouds) {
            cloudCanvases[cloudType] = new SpriteCanvasCacher({
                sprite: entrySprite,
                position: clouds[cloudType]
            });
        }

        // 
        var rainDropCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, 
            position: particles.rain
        });
        var snowCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, 
            position: particles.snow
        });
        var snowCanvas2 = new SpriteCanvasCacher({
            sprite: entrySprite, 
            position: particles.snow2
        });


        // Sun canvas
        var sunCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.sun
        });
        
        // Land Block canvases
        var landBlock1 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land1
        });
        var landBlock2 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land2
        });
        var landBlock3 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land3
        });
        var landBlock4 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.land4
        });
        var landBlocksCanvases = [ landBlock1, landBlock2, landBlock3, landBlock4 ];

        // Crates
        var crateBlock1 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.crate1
        });
        var crateBlock2 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.crate2
        });
        
        // Trees
        var tree1 = new SpriteCanvasCacher({
            sprite: tree, position: tree.tree
        });
        



        var characterCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.character
        });
        var waterCanvas = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.water
        });
        var waterBaseCanvasLevel1 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.waterBaseLevel1
        });
        var waterBaseCanvasLevel2 = new SpriteCanvasCacher({
            sprite: entrySprite, position: entrySprite.waterBaseLevel2
        });


        // Foreground
        var foreground = new Foreground({
            foreground: "#foreground",
            characterImage: "http://bit.ly/1lA252j",
            game: game
        });

        // Background
        var background = new Background({
            background: "#background",
            landImage: "http://bit.ly/1gkZGR9",
            game: game,
            canvas: backgroundCanvases.normal, canvases: backgroundCanvases
        });


        var entities = [];
        
        // Base Block objects
        for (var i = 0; i < game.settings.w + game.settings.dw; i+= game.settings.dw) {
            
            var newBlock;
        
            if (i < Math.ceil(game.settings.w * 0.5) ) {
                // First 50% of screen are the land blocks
                newBlock = getLandBlock(i, game.getBaseBlockTop() );
            } else {
                // Next is random
                newBlock = getRandomBlock(i, game.getBaseBlockTop() );
            }
            
            entities.push({
                entity: new Block(newBlock),
                type: "baseBlocks"
            });
        }

        // Sun Object
        entities.push({
            entity: new Sun({   game : game, canvas:   sunCanvas, minHeight: 10, maxHeight: 0 }),
            type: "sun"
        });

        // Cloud Objects
        for (var c = 0; c < 20; c++) {
            entities.push({
                entity: new Cloud({ 
                    game : game, canvas: cloudCanvases.normal, 
                    minHeight: getRandom(15, 45), maxHeight: getRandom(0, 15), canvases: cloudCanvases
                }),
                type:"cloud"
            });
        }

        // Character Object
        entities.push({
            entity: new Character({
                game: game, 
                canvas: characterCanvas,
                x: game.settings.dw,
                // Above the base block
                y: game.getBaseBlockTop() - game.settings.dh,
                direction: "CENTER",
                onGround: true,
                XSpeed: 2,
                YSpeed: 2,
                gravityXSpeed: 2,
                gravityYSpeed: 1,
                gravityTime: 62,
                percentAtWhichStageMoves: 0.5
            }),
            type: "character"
        });

        // 2 second-level parallax water blocks that follow each other
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel2,
                game: game, 
                type: "baseWater",
                x: -40,
                y: game.getBaseBlockTop() + 12,
                level: 2
            }),
            type: "parallaxWater2",
        });
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel2,
                game: game, 
                type: "baseWater",
                x: -40 + waterBaseCanvasLevel2.canvasW,
                y: game.getBaseBlockTop() + 12,
                level: 2
            }),
            type: "parallaxWater2",
        });
        // 2 first-level parallax water blocks that follow each other
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel1,
                game: game, 
                type: "baseWater",
                x: 0,
                y: game.getBaseBlockTop() + 20,
                level: 1
            }),
            type: "parallaxWater",
        });
        entities.push({
            entity: new Block({
                canvas: waterBaseCanvasLevel1,
                game: game, 
                type: "baseWater",
                x: waterBaseCanvasLevel1.canvasW,
                y: game.getBaseBlockTop() + 20,
                level: 1
            }),
            type: "parallaxWater",
        });




        function getLandBlock (x, y) {
            var randomBlockNum = getRandom(0, landBlocksCanvases.length-1);
            return {
                game: game, 
                canvas: landBlocksCanvases[randomBlockNum],
                type: "land",
                x: x, y: y
            };
        }

        function getWaterBlock (x, y, sequence) {
            return {
                game: game, 
                canvas: waterCanvas,
                type: "water",
                x: x,
                y: y,
                sequence: sequence || 1
            };
        }

        function getRandomBlock (x, y) {
            var baseBlocks = foreground.entities.baseBlocks || [],
                isLand = getPercentRandom(90),
                lastBlock = baseBlocks[baseBlocks.length - 1],
                sequence = 1;
            
            if (baseBlocks && lastBlock && lastBlock.type == "water") {
                isLand = getPercentRandom(40);
                sequence = lastBlock.settings.sequence + 1;
                if (sequence > game.settings.maxWaterBlocks) isLand = true;
            }

            if (isLand) {
                return getLandBlock(x, y);
            } else {
                return getWaterBlock(x, y, sequence);
            }
        }

        function getBlocksBeneathOtherBlock (otherBlock, blocks) {
            
                // Get block's left side
            var blockLeftSide = otherBlock.x,
                // Get block's right side
                blockRightSide = blockLeftSide + game.dw,
                // Get block's bottom
                blockBottom = otherBlock.y + game.dh,
                // Get the block difference from our block
                allBlocksLeftSide = blocks[0].x,
                remainderFromBlockWidth = (blockLeftSide ? blockLeftSide % game.dw : 0),
                blockDifference = allBlocksLeftSide > 0 ? 
                                    remainderFromBlockWidth - allBlocksLeftSide :
                                    remainderFromBlockWidth + Math.abs(allBlocksLeftSide),
                minimumBlockX,
                maximumBlockX;

                minimumBlockX = blockLeftSide - blockDifference;
                maximumBlockX = blockLeftSide + blockDifference;
                if (maximumBlockX < blockRightSide) maximumBlockX += game.dw;

                var filtered =  _.filter(blocks, function (b) {
                    return b.x >= minimumBlockX && b.x <= maximumBlockX && b.y == blockBottom;
                });

                return filtered;
        }
        
        function getHPercentOfTwoBlock (block1, block2) {
            return 1 - (Math.abs(block1.x - block2.x) / game.dw);
        }

        function checkBlocksBeneathCharacterAreLand (block, blocks) {
            
            var blocksBeneath = getBlocksBeneathOtherBlock(block, blocks),
                landType = ["land"],
                //thirdOfABlock = Math.floor(game.dw * 0.33),
                isLandOne = (blocksBeneath[0] && landType.indexOf(blocksBeneath[0].type) > -1 ),
                isLandTwo = (blocksBeneath[1] && landType.indexOf(blocksBeneath[1].type) > -1 ),
                percentOfCharacterOnBlockOne;
            
            // console.log(
            //     blocksBeneath[0] ? blocksBeneath[0].x : "", blocksBeneath[0] ? blocksBeneath[0].y : "",              
            //     blocksBeneath[1] ? blocksBeneath[1].x : "", blocksBeneath[1] ? blocksBeneath[1].y : "" );
            
            if (blocksBeneath.length <= 0) {
                // No blocks
                
                return false;
            } else if (blocksBeneath.length == 1) {
                // If character stands on one block
                //console.clear();
                //console.table("Only 1 block", getHPercentOfTwoBlock(block, blocksBeneath[0]), "<" );
                
                if (getHPercentOfTwoBlock(block, blocksBeneath[0]) > 0.33) {
                    return isLandOne;
                }
                return false;
            } else {
                
                //console.clear();
                //console.table(blocksBeneath);
                // If character stands on multiple blocks

                // If both blocks are land
                if (isLandOne && isLandTwo) return true;

                if (getHPercentOfTwoBlock(block, blocksBeneath[0]) > 0.33) {
                    return isLandOne;
                } else {
                    return isLandTwo;
                }
            }
            return false;
        }

        function moveBlocks(blockEntities, callback) {
            if (blockEntities && blockEntities.length) {            
                var newTopBlock,
                    oneBlockWidth = blockEntities[0].settings.canvas.canvasW || 
                                    blockEntities[0].settings.canvas.w,
                    lastBlockRightSide;

                // entity is out of canvas
                if (blockEntities[0].x + oneBlockWidth <= 0) {

                    // Remove the first block
                    newTopBlock = blockEntities.shift();

                    // Get the last block's right side
                    lastBlockRightSide = blockEntities[blockEntities.length - 1].x + oneBlockWidth;
                                    
                    // Place the removed block to the right side
                    newTopBlock.x = lastBlockRightSide;

                    // if needed, setup block settings
                    if (callback) newTopBlock = callback(newTopBlock, blockEntities, lastBlockRightSide);

                    // push offset block to the back of block entities array
                    blockEntities.push(newTopBlock);
                }
            }
        }

        function removeBlocksOutOfCanvas(blockEntities) {
            if (blockEntities && blockEntities.length) {
                console.clear();
                // console.log(blockEntities);
                var oneBlockWidth = blockEntities[0].settings.canvas.canvasW || 
                    blockEntities[0].settings.canvas.w;

                if (blockEntities[0].x + oneBlockWidth <= 0) {
                    // Remove the block
                    blockEntities.shift();
                }
            }
        }

        function setCharacterDirectionFromPressedKeys() {
            // Setup character's current direction based on keys that are pressed
            var character = foreground.entities.character[0];
            if (!_.some(Key.pressed)) {
                // keep previous direction, or go down
                character.direction = "DOWN";
            } else {
                character.direction = "";
                     if (Key.isDown(Key.RIGHT)) character.direction  = "RIGHT";
                else if (Key.isDown(Key.LEFT))  character.direction  = "LEFT";
                     if (Key.isDown(Key.UP))    character.direction += "UP";
                else if (Key.isDown(Key.DOWN))  character.direction += "DOWN";
            }
        }


        // Character moved. 
        // Check if block beneath is a land block.
        // Setup character direction from currently pressed keys
        game.PubSub.subscribe("Character:Move", function () {
            var character = foreground.entities.character[0],
                // for now, add more types later
                allBlocks = foreground.entities.baseBlocks.concat(foreground.entities.crates ? foreground.entities.crates : []);
            
            character.blockBeneathIsLand = checkBlocksBeneathCharacterAreLand(character, allBlocks);
            setCharacterDirectionFromPressedKeys();
        });

        // Character moved to the right.
        // Start moving the blocks, water and other moving elements
        game.PubSub.subscribe("Character:Move:Right", function () {

            moveBlocks(
                foreground.entities.baseBlocks,
                function (newBlock, blockEntities, lastBlockRightSide) {
                    var newBlockSettings = getRandomBlock(lastBlockRightSide, newBlock.y);
                    newBlock.setSettings(newBlockSettings);
                    return newBlock;
                }
            );

            moveBlocks(foreground.entities.parallaxWater);
            moveBlocks(foreground.entities.parallaxWater2);


            var baseBlocks = foreground.entities.baseBlocks,
                lastBaseBlock = baseBlocks[baseBlocks.length - 1],
                duh = lastBaseBlock.x % game.dw,
                shouldAddCrate = getPercentRandom(5),
                shouldAddTree = getPercentRandom(8);
                

            if (!duh && lastBaseBlock.type == "land" && shouldAddCrate) {                
                foreground.addEntity(
                    new Block({
                        x: lastBaseBlock.x,
                        y: lastBaseBlock.y - game.dh,
                        game: game,
                        canvas: (getRandom(0, 1) ? crateBlock1 : crateBlock2),
                        type: "land"
                    }),
                    "crates"
                );
            } else if (!duh && lastBaseBlock.type == "land" && shouldAddTree) {
                foreground.addEntity(
                    new Block({
                        x: lastBaseBlock.x,
                        y: lastBaseBlock.y - game.dh,
                        game: game,
                        canvas: tree1,
                        type: "land"
                    }),
                    "trees"
                );                
            }

            removeBlocksOutOfCanvas(foreground.entities.crates);

        });


        // It is time to change the weather!
        game.PubSub.subscribe("Game:ChangeWeatherTime", function () {

            var allWeather = _.keys(game.weather),
                // figure out what meteorologists say the weather will be like
                newWeather = allWeather[getRandom(0, allWeather.length-1)];
            
            // console.log("changing weather!", newWeather);

            game.PubSub.notify("Game:ChangeWeather", { 
                weather: newWeather,
                time: game.changeWeatherTime
            });

        });

        // Cloud Changes Weatehr
        game.PubSub.subscribe("Cloud:ChangeWeather", function (object) {
            
            var cloud = object.cloud,
                x = cloud.x,
                y = cloud.y + (game.dh / 2),
                thirdBlockWidth = game.dw / 3,
                weather = object.weather;

            // console.log("adding rain drops");

            if (weather == "rain") {            
                
                // spawn 3 rain particles
                for (var i = 0; i < 3; i++) {
                    
                    foreground.addEntity(new Rain({
                        x: x + (thirdBlockWidth * i),
                        // right from the middle of the cloud
                        y: y,
                        canvas: rainDropCanvas
                    }));
                }

            } else if (weather == "snow") {

                // spawn 2 snow particles
                for (var j = 0; j < 2; j++) {
                    
                    foreground.addEntity(new Rain({
                        x: x + (thirdBlockWidth * j),
                        // right from the middle of the cloud
                        y: y,
                        canvas: (getRandom(0, 1) ? snowCanvas : snowCanvas2)
                    }));
                }

            }


        });





        // Adds base blocks to the foreground
        // Adds sun to the foreground
        // Adds a bunch of clouds to the foreground
        // Adds character to the foreground
        // Adds 2 level 2 parallax waves
        // Adds 2 level 1 parallax waves
        entities.forEach(function (entity) {
            foreground.addEntity(entity.entity, entity.type);
        });

        // Add canvases to game
        game.addStage(background)
            .addStage(foreground)
            .startGame();

        game.PubSub.subscribe("Character:Dead", function () {
            game.endGame();
        });




        document.querySelector(".newGame").onclick = function () {
            game.endGame();
            console.log("new game");
            game.startGame();
        };

        // Keyboard keys pressed
        document.onkeydown = document.onkeyup = function (event) { 
            Key.onKeyDown(event);
        };

        console.clear();
        console.log("New Game");
    };

});
var Key = {
    pressed : [],
    LEFT  : 37,
    RIGHT : 39,
    DOWN  : 40,
    UP    : 38,
	isDown : function (keycode) {
        return this.pressed[keycode];
	},
	onKeyDown : function (e) {
        this.pressed[e.keyCode] = e.type == 'keydown';  
	}
};

// PubSub
var PubSub = {
    
    actionCallbacks: [],
    
    subscribe : function (actionName, callback) {
        if (!this.actionCallbacks[actionName]) {
            this.actionCallbacks[actionName] = [];
        }
        this.actionCallbacks[actionName].push(callback);
    },
    
    notify: function (actionName) {
        var args = Array.prototype.splice.call(arguments, 1),
            applyCallback = function (callback) {
                // call back the callbacks that were added to PubSub for this action Name.
                // console.log('Notifying ', actionName, " with these args: ", args);
                callback.apply(null, args);
            };

        for (var i in this.actionCallbacks) {
            if (this.actionCallbacks.hasOwnProperty(i) && i === actionName) {
                this.actionCallbacks[i].forEach(applyCallback);
            }
        }
    }

};
/* global _ */

var Rain = function (settings) {
    var self = this;
    self.setSettings(settings);
};

Rain.prototype.setSettings = function(settings) {
    var self = this;
    self.settings = settings;
    _.extend(self, settings);
};

Rain.prototype.getRainImageWidth = function () {
    return this.settings.canvas.w;
};

Rain.prototype.getClientWidth = function () {
    return this.settings.game.settings.w;
};

Rain.prototype.move = function () {
    var self = this;
    self.y++;
};

Rain.prototype.draw = function (context) {
	var self = this,
		position = self.settings.canvas;
	self.move();
	context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};
/* global _ */

(function (_) {

    var root = this,
        spritesCached = [];

    var SpriteCanvasCacher = function (settings) {

        var self = this,
            foundSprite,
            foundPosition;

        spritesCached.forEach(function (sc) {
            if (sc.sprite == settings.sprite) {
                foundSprite = sc;
                foundPosition = _.find(sc.positions, settings.position);
            }
        });

        if (!foundSprite) {

            // console.log("NOT FOUND");

            // Sprite not found, add new one

            foundPosition = self.setNewPosition(settings.position);
            self.tryDrawing(settings.sprite, foundPosition);
        
            // cache the sprite and this first position, with it's canvas
            spritesCached.push({
                sprite: settings.sprite,
                positions: [foundPosition]
            });

        } else {
            // Sprite Found

            if (!foundPosition) {
                // Position Not Found, add new one
                foundPosition = self.setNewPosition(settings.position);
                self.tryDrawing(foundSprite.sprite, foundPosition);
                foundSprite.positions.push(foundPosition);
            } 
       }

        return foundPosition;
    };


    SpriteCanvasCacher.prototype.setCanvasContext = function (position) {
        var canvas    = document.createElement("canvas"),
            context   = canvas.getContext("2d");
        canvas.width  = position.canvasW || position.w;
        canvas.height = position.canvasH || position.h;
        return { canvas: canvas, context: context };
    };

    SpriteCanvasCacher.prototype.setNewPosition = function (position) {
        var self = this,
            canvasContext = self.setCanvasContext(position);
        position.canvas   = canvasContext.canvas;
        position.context  = canvasContext.context;
        return position;
    };

    SpriteCanvasCacher.prototype.tryDrawing = function (sprite, position) {
        var self = this;

        if (position.type == "image") {
            // console.log("drawing image on a canvas");
            // console.log("Sprite",sprite);
            // Draw an image on a canvas
            position.context.drawImage(
                sprite,
                position.x, position.y, 
                position.w, position.h,
                // on the canvas at 0, 0 for w, h
                0, 0,
                position.w, position.h);
        
        } else if (position.type == "pattern") {

            console.log("here", sprite.width, sprite.height);

            // 1. draw image at x, y for w, h
            position.context.drawImage(
                sprite,
                // clip w, h at x, y
                position.x, position.y, 
                position.w, position.h,
                // on the canvas at 0, 0 for w, h
                0, 0,
                position.w, position.h);

            // 2. Obtain new image data at x, y for w, h
            var image = position.context.getImageData(
                0, 0, 
                position.w, position.h
            );

            // 3. Make image canvas
            var imageCanvas = self.setCanvasContext({w: position.w, h: position.h});

            // 4. Put image onto image canvas at 0 0      
            imageCanvas.context.putImageData(image, 0, 0);

            // 5. Clear canvas 
            position.context.clearRect(0, 0, 
                position.clientW || position.w,
                position.clientH || position.h
                );

            // 6. Create pattern element from image canvas
            position.pattern = position.context.createPattern(
                imageCanvas.canvas, 
                position.direction || "repeat"
            );

            position.context.fillStyle = position.pattern;
            
            position.context.fillRect(0, 0, 
                position.canvasW || position.w, 
                position.canvasH || position.h
            );
            //position.context.fill();

            position.w = position.canvasW;

        } else if (position.type == "color") {
            // console.log(position.color,position.x, position.y, position.w, position.h)
            position.context.fillStyle = position.color;
            position.context.fillRect(position.x, position.y, position.w, position.h);
        }
    };

    root.SpriteCanvasCacher = SpriteCanvasCacher;

}).call(this, _);
/* global getRandom */

var Sun = function (settings) {
    var self = this;
    self.settings = settings;

    self.x = getRandom(0, self.getSunImageWidth() - 100);
    self.y = getRandom(self.settings.maxHeight, self.settings.minHeight);
    // self.speed = 0.2;
    self.time = 0;
    self.minHeight = self.settings.minHeight;
    self.maxHeight = self.settings.maxHeight;
};

Sun.prototype.getSunRandomSpeed = function () {
return getRandom(0.2, 1.1);
};

Sun.prototype.getSunImageWidth = function () {
    return this.settings.canvas.w;
};

Sun.prototype.getClientWidth = function () {
    return this.settings.game.settings.w;
};

Sun.prototype.move = function (time) {
    var self = this,
        sunImageWidth = self.getSunImageWidth();
    if (time - self.time  >= 60) {
        self.time = time;
        self.x--;
    }
    if (self.x <= -(sunImageWidth)) {
        self.x = self.getClientWidth() + sunImageWidth;
    }
};

Sun.prototype.draw = function (context, time) {
    var self = this,
        position = self.settings.canvas;
    self.move(time);
    context.drawImage(self.settings.canvas.canvas, self.x, self.y, position.w, position.h);
};